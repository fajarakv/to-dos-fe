import React, { useEffect } from "react";
import Login from "./pages/Login";
import Register from "./pages/Register";
import HomePage from "./pages/HomePage";
import Navbar from "./components/navbar";
import Footer from "./components/Footer";
import NotFound from "./pages/404";
import PrivateRoute from "./components/privateRoute";

import "./App.css";

import { useNavigate } from "react-router";
import { Route, Routes, Navigate } from "react-router-dom";

const App = () => {
  const navigate = useNavigate();
  const token = localStorage.getItem("token");
  const pathname = window.location.pathname;

  useEffect(() => {
    if (token && pathname === "/login") navigate("/");
  }, [pathname]);

  return (
    <React.Fragment>
      <div className="container">
        {window.location.pathname !== "/login" &&
          window.location.pathname !== "/404" &&
          window.location.pathname !== "/register" && <Navbar />}
        <Routes>
          <Route path="register" element={<Register />} />
          <Route path="login" element={<Login />} />
          <Route
            path="/"
            element={
              <PrivateRoute>
                <HomePage />
              </PrivateRoute>
            }
          />
          <Route path="404" element={<NotFound />} />
          <Route path="*" element={<Navigate to="/404" replace />} />
        </Routes>
        {window.location.pathname !== "/login" &&
          window.location.pathname !== "/register" &&
          window.location.pathname !== "/404" && <Footer />}
      </div>
    </React.Fragment>
  );
};

export default App;
