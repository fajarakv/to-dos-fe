import { configureStore } from "@reduxjs/toolkit";
import loginReducer from "../pages/Login/reducer";
import registerReducer from "../pages/Register/reducer";
import todosReducer from "../pages/HomePage/reducer";

export const store = configureStore({
  reducer: {
    login: loginReducer,
    register: registerReducer,
    todos: todosReducer,
  },
});
