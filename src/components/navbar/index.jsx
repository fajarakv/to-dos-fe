import "./navbar.css";
import imgLogo from "../../assets/Todo.png";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router";
import React from "react";

function Navbar() {
  const auth = localStorage.getItem("token");
  const navigate = useNavigate();
  const logout = () => {
    localStorage.clear();
    navigate("/login");
  };
  const handleClick = (link) => {
    navigate(link);
  };

  return (
    <header>
      <div className="logo-container">
        <img src={imgLogo} alt="Logo" onClick={() => handleClick("/")} />
        <span>To Dos</span>
      </div>
      <nav>
        <button className="nav-btn">
          <Link onClick={logout} to="/login">
            Logout
          </Link>
        </button>
      </nav>
    </header>
  );
}

export default Navbar;
