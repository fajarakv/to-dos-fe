import React from "react";
import "./footer.css";

function Footer() {
  return (
    <div className="footer-container">
      <div className="footer">
        <div className="footer-text">
          <p>Fajar Akvianto Pratama © 2023</p>
        </div>
      </div>
    </div>
  );
}

export default Footer;
