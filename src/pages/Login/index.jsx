import { TextField, Button, CardContent, Alert } from "@mui/material";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router";
import "./login.css";
import LogoImg from "../../assets/Todo.png";
import LoginImg from "../../assets/login.png";
import { useSelector, useDispatch } from "react-redux";
import { loginPost } from "./reducer";
import { Link } from "react-router-dom";

const Login = () => {
  const [pin, setPin] = useState("");
  const [error, setError] = useState("");
  const Navigate = useNavigate();
  const dispatch = useDispatch();
  const loginErr = useSelector((state) => state.login.error);

  const handleLogin = () => {
    const payload = { pin };
    dispatch(loginPost(payload))
      .unwrap()
      .then(() => {
        Navigate("/");
      });
  };

  const handleClick = (link) => {
    Navigate(link);
  };

  useEffect(() => {
    setError(loginErr);
    setTimeout(() => {
      setError("");
    }, 3000);
  }, [loginErr]);

  const handleKeypress = (e) => {
    if (e.keyCode === 13) {
      handleLogin();
    }
  };

  return (
    <div className="app-container">
      <CardContent
        sx={{
          paddingBottom: "100px",
          fontSize: "20px",
          width: { lg: 400, xl: 500 },
          justifyContent: "center",
        }}
      >
        <img
          src={LogoImg}
          className="logoo"
          onClick={() => handleClick("/")}
        ></img>
        <h1 className="login">Login</h1>
        <p>Login to access all the features</p>

        <TextField
          sx={{ marginTop: "15px" }}
          label="PIN"
          type="password"
          variant="standard"
          fullWidth
          value={pin}
          onChange={(e) => setPin(e.target.value)}
          onKeyUp={handleKeypress}
        />
        <Button
          sx={{
            color: "white",
            backgroundColor: "black",
            borderRadius: "15px",
            marginTop: "30px",
            width: "100%",
            maxWidth: "400px",
          }}
          onClick={handleLogin}
        >
          Log in
        </Button>
        <div className="signin">
          <span>
            doesn&apos;t have an account?{" "}
            <Link to="/register">register here</Link>
          </span>
        </div>
        {error && (
          <Alert sx={{ marginTop: "10px" }} severity="error">
            {error}
          </Alert>
        )}
      </CardContent>
      <div className="right">
        <img src={LoginImg} alt=""></img>
      </div>
    </div>
  );
};

export default Login;
