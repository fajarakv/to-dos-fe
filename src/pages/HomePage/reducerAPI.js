import axios from "axios";

export const getTodosAPI = async () => {
  const userId = localStorage.getItem("userId");

  if (!userId) {
    throw new Error("User ID not found in local storage");
  }
  const res = await axios.get(`${process.env.REACT_APP_BE_URL}todos/${userId}`);
  return res;
};

export const postTodosAPI = async (payload) => {
  const userId = localStorage.getItem("userId");

  if (!userId) {
    throw new Error("User ID not found in local storage");
  }
  const res = await axios.post(
    `${process.env.REACT_APP_BE_URL}todos/${userId}`,
    payload
  );

  return res;
};

export const deleteTodosAPI = async (id) => {
  const userId = localStorage.getItem("userId");

  if (!userId) {
    throw new Error("User ID not found in local storage");
  }
  const res = await axios.delete(
    `${process.env.REACT_APP_BE_URL}todos/${userId}/${id}`
  );

  return res;
};

export const updateTodosAPI = async (id, payload) => {
  const userId = localStorage.getItem("userId");

  if (!userId) {
    throw new Error("User ID not found in local storage");
  }
  const res = await axios.put(
    `${process.env.REACT_APP_BE_URL}todos/${userId}/${id}`,
    payload
  );

  return res;
};
