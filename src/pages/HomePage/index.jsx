import * as React from "react";
import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getTodos, data, postTodos, deleteTodos, updateTodos } from "./reducer";
import {
  Alert,
  Card,
  Box,
  CardActions,
  Typography,
  CardContent,
  Button,
  Grid,
  TextField,
  Container,
  Checkbox,
  IconButton,
  Tooltip,
} from "@mui/material";
import DeleteIcon from "@material-ui/icons/Delete";
import Check from "@material-ui/icons/Check";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import "./home.css";

const HomePage = () => {
  const user = localStorage.getItem("name");
  const [todo, setTodo] = useState("");
  const [error, setError] = useState("");
  const [selectedItems, setSelectedItems] = useState([]);
  const todos = useSelector(data);

  const dispatch = useDispatch();
  const postErr = useSelector((state) => state.todos.error);

  const handlePost = () => {
    const payload = { todo };
    dispatch(postTodos(payload))
      .unwrap()
      .then(() => {
        setTodo("");
        handleFetch();
      })
      .catch((err) => {
        console.error(err);
      });
  };

  useEffect(() => {
    setError(postErr);
    setTimeout(() => {
      setError("");
    }, 3000);
  }, [postErr]);

  const handleFetch = () => {
    dispatch(getTodos());
  };

  useEffect(() => {
    handleFetch();
  }, []);

  const handleDelete = (id) => {
    const idList = Array.isArray(id) ? id : [id];

    Promise.all(idList.map((id) => dispatch(deleteTodos(id)).unwrap()))
      .then(() => {
        setSelectedItems(
          selectedItems.filter((itemId) => !idList.includes(itemId))
        );
        handleFetch();
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const handleUpdate = (id) => {
    Promise.all(
      id.map((id) => dispatch(updateTodos({ id, completed: true })).unwrap())
    )
      .then(() => {
        setSelectedItems([]);
        handleFetch();
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const handleCheckboxChange = (itemId) => {
    if (selectedItems.includes(itemId)) {
      setSelectedItems(selectedItems.filter((id) => id !== itemId));
    } else {
      setSelectedItems([...selectedItems, itemId]);
    }
  };

  const handleKeypress = (e) => {
    if (e.keyCode === 13) {
      handlePost();
    }
  };

  return (
    <Container>
      <Box my={5}>
        <Typography variant="h4" component="h1">
          {user} To Do List
        </Typography>
        <Box display="flex" alignItems="center" my={1}>
          <TextField
            label="Todo"
            variant="outlined"
            size="small"
            fullWidth
            value={todo}
            onChange={(e) => setTodo(e.target.value)}
            onKeyUp={handleKeypress}
            sx={{ height: "auto" }}
          />
          <IconButton
            aria-label="add"
            color="secondary"
            onClick={handlePost}
            sx={{ width: "auto" }}
          >
            <AddCircleIcon fontSize="large" />
          </IconButton>
        </Box>
        <Box>
          <Button
            variant="contained"
            startIcon={<Check />}
            color="success"
            onClick={() => handleUpdate(selectedItems)}
            disabled={selectedItems.length === 0}
            sx={{ width: "fit-content", marginRight: "10px" }}
          >
            Completed ({selectedItems.length})
          </Button>
          <Button
            variant="contained"
            startIcon={<DeleteIcon />}
            color="error"
            onClick={() => handleDelete(selectedItems)}
            disabled={selectedItems.length === 0}
            sx={{ width: "auto" }}
          >
            Delete It ({selectedItems.length})
          </Button>
        </Box>
        {error && (
          <Alert sx={{ marginTop: "10px" }} severity="error">
            {error}
          </Alert>
        )}
      </Box>
      <Grid container spacing={3} my={3}>
        {Array.isArray(todos?.data?.data) ? (
          todos?.data?.data?.map((row, i) => (
            <Grid key={i} item xs={4}>
              <Card
                sx={{
                  backgroundColor: row.completed ? "#2e7d32" : "inherit",
                }}
              >
                <CardContent
                  sx={{
                    color: row.completed ? "white" : "inherit",
                  }}
                >
                  <Tooltip title={row?.todo ?? "-"} placement="bottom-start">
                    <Typography
                      variant="body1"
                      gutterBottom
                      sx={{
                        textOverflow: "ellipsis",
                        overflow: "hidden",
                        whiteSpace: "nowrap",
                        textDecoration: "none",
                      }}
                      component="div"
                    >
                      {row?.todo ?? "-"}
                    </Typography>
                  </Tooltip>
                </CardContent>
                <CardActions>
                  <Checkbox
                    checked={selectedItems.includes(row?.id)}
                    onChange={() => handleCheckboxChange(row?.id)}
                    inputProps={{ "aria-label": "controlled" }}
                    disabled={row?.completed}
                    sx={{
                      color: row.completed ? "white" : "inherit",
                    }}
                  ></Checkbox>
                  <IconButton
                    aria-label="delete"
                    color="error"
                    onClick={() => handleDelete(row?.id)}
                    sx={{
                      width: "auto",
                      color: row.completed ? "white" : "error",
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </CardActions>
              </Card>
            </Grid>
          ))
        ) : (
          <Card style={{}}>
            <CardContent>No Data</CardContent>
          </Card>
        )}
      </Grid>
      {todos.status === "loading" && <Typography>Loading</Typography>}
    </Container>
  );
};

export default HomePage;
