import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import * as API from "./reducerAPI";

const initialState = {
  show: {},
  data: [],
  isLoading: false,
  error: "",
  success: "",
  status: "idle",
};

export const getTodos = createAsyncThunk("getTodos/data", async () => {
  const res = await API.getTodosAPI();

  return res.data;
});

export const postTodos = createAsyncThunk(
  "postTodos/",
  async (payload, thunkAPI) => {
    try {
      const res = await API.postTodosAPI(payload);

      return res.data;
    } catch (err) {
      return thunkAPI.rejectWithValue(err.response.data.msg);
    }
  }
);

export const deleteTodos = createAsyncThunk(
  "deleteTodos/",
  async (id, thunkAPI) => {
    try {
      const res = await API.deleteTodosAPI(id);

      return res.data;
    } catch (err) {
      return thunkAPI.rejectWithValue(err.response.data.msg);
    }
  }
);

export const updateTodos = createAsyncThunk(
  "updateTodos/",
  async ({ id, completed }, thunkAPI) => {
    try {
      const res = await API.updateTodosAPI(id, { completed });

      return res.data;
    } catch (err) {
      return thunkAPI.rejectWithValue(err.response.data.msg);
    }
  }
);

export const todosSlice = createSlice({
  name: "todos",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getTodos.pending, (state) => {
        state.status = "loading";
        state.error = "";
        state.success = "";
      })
      .addCase(getTodos.rejected, (state, action) => {
        state.status = "error";
        state.error = action.payload;
      })
      .addCase(getTodos.fulfilled, (state, action) => {
        state.status = "success";
        state.data = action.payload;
        state.success = action.payload.status;
      })
      .addCase(postTodos.pending, (state) => {
        state.status = "loading";
        state.error = "";
        state.success = "";
      })
      .addCase(postTodos.rejected, (state, action) => {
        state.status = "error";
        state.error = action.payload;
      })
      .addCase(postTodos.fulfilled, (state, action) => {
        state.status = "success";
        state.data = action.payload;
        state.success = action.payload.status;
      })
      .addCase(deleteTodos.pending, (state) => {
        state.status = "loading";
        state.error = "";
        state.success = "";
      })
      .addCase(deleteTodos.rejected, (state, action) => {
        state.status = "error";
        state.error = action.payload;
      })
      .addCase(deleteTodos.fulfilled, (state, action) => {
        state.status = "success";
        state.data = action.payload;
        state.success = action.payload.status;
      })
      .addCase(updateTodos.pending, (state) => {
        state.status = "loading";
        state.error = "";
        state.success = "";
      })
      .addCase(updateTodos.rejected, (state, action) => {
        state.status = "error";
        state.error = action.payload;
      })
      .addCase(updateTodos.fulfilled, (state, action) => {
        state.status = "success";
        state.data = action.payload;
        state.success = action.payload.status;
      });
  },
});

export const data = (state) => state.todos;

export default todosSlice.reducer;
