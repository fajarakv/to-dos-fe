import axios from "axios";

export const registerAPI = async (payload) => {
  const res = await axios.post(
    `${process.env.REACT_APP_BE_URL}auth/register`,
    payload
  );

  return res;
};
