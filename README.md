# To Dos Frontend

To Dos Frontend is a web application that allows users to manage and organize their todos or tasks effectively. It provides a user-friendly interface for creating, updating, and deleting todos.

## Features

- User Registration with Name and PIN
- User Login with PIN
- Create a new to-do item
- Retrieve a list of all to-do items based on user
- Update a to-do item to completed status
- Delete a to-do item

## Usage

Open your web browser and visit http://54.206.100.220:3000/ to view the deployed project.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Answer

1. Apakah Kegunaan JSON pada REST API?

JSON (JavaScript Object Notation) adalah format data yang umum digunakan dalam REST API. JSON memungkinkan pertukaran data antara client dan server secara efisien dan dapat dibaca oleh manusia serta mudah diproses oleh komputer.

Pada REST API, JSON digunakan untuk mengirim dan menerima data antara client dan server. Saat client melakukan permintaan (request) ke server, data yang dikirim dalam payload permintaan atau respons (request/response) umumnya berbentuk JSON.

Dengan menggunakan JSON, REST API dapat mentransmisikan data dengan format yang seragam dan baik. JSON juga mendukung nested structures, sehingga memungkinkan pengiriman data yang kompleks seperti objek dalam objek atau array dalam objek.

2. Jelaskan bagaimana REST API bekerja?

REST (Representational State Transfer) adalah arsitektur komunikasi yang digunakan dalam pengembangan web services. REST API memungkinkan client (misalnya aplikasi atau website) untuk berinteraksi dengan server menggunakan protokol HTTP. Berikut adalah langkah-langkah umum tentang bagaimana REST API bekerja:

1.Client melakukan request

2.Server menerima request

3.Server memproses permintaan

4.Server membuat response

5.Client menerima response

6.Interaksi berakhir
